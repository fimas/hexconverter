#include <stdio.h>
#include <stddef.h> // For size_t
#include "converter.h"

int main() {
    // Start by testing converting a number to a hexadecimal string
    printf("Testing converting a number to a hexadecimal string\n");
    unsigned long num = 2882400018L;
    const size_t bufferSize = 9;
    char hexStr[bufferSize];

    if (convertNumToHexStr(hexStr, bufferSize, num)) {
        printf("Hexadecimal string: %s\n", hexStr);
    } else {
        printf("Error in conversion\n");
        return 1;
    }

    // Now test converting a number to a hexadecimal string manually
    printf("\nTesting converting a number to a hexadecimal string manually\n");
    unsigned long num1 = 2882400018L;
    const size_t bufferSize1 = 9;
    char hexStr1[bufferSize1];

    if (manualConvertionNumToHexStr(hexStr1, bufferSize1, num1)) {
        printf("Hexadecimal string: %s\n", hexStr1);
    } else {
        printf("Error in conversion\n");
        return 1;
    }

    // Now test converting a hexadecimal string to a number
    printf("\nTesting converting a hexadecimal string to a number\n");
    unsigned long num2;
    char hexStr2[] = "ABCDEF12";

    if (convertHexStrToNum(&num2, hexStr2)) {
        printf("Number: %lu\n", num2);
    } else {
        printf("Error in conversion\n");
        return 1;
    }

    // Finally test converting a hexadecimal string to a number manually
    printf("\nTesting converting a hexadecimal string to a number manually\n");
    unsigned long num3;
    char hexStr3[] = "ABCDEF12";

    if (manualConvertionHexStrToNum(&num3, hexStr3)) {
        printf("Number: %lu\n", num3);
    } else {
        printf("Error in conversion\n");
        return 1;
    }

    return 0;
}
