# Hexconverter

This repository contains functions to convert numbers to hexadecimal strings and vice versa. The functions are implemented in `converter.c` and can be tested using the main file provided.

## Functions

### convertNumToHexStr

```c
int convertNumToHexStr(char *hexStr, size_t bufferSize, unsigned long num);
```

This function converts a given number to a hexadecimal string. It uses the snprintf function to perform the conversion.

* `hexStr`: A pointer to the buffer where the resulting hexadecimal string will be stored.
* `bufferSize`: The size of the buffer.
* `num`: The number to convert.

Returns the number of characters written to the buffer (excluding the null terminator) or 0 if the buffer is not large enough.

### manualConvertionNumToHexStr

```c
int manualConvertionNumToHexStr(char *hexStr, size_t bufferSize, unsigned long num);
```

This function manually converts a given number to a hexadecimal string.

* `hexStr`: A pointer to the buffer where the resulting hexadecimal string will be stored.
* `bufferSize`: The size of the buffer.
* `num`: The number to convert.

Returns the number of characters written to the buffer (excluding the null terminator) or 0 if the buffer is not large enough.

### convertHexStrToNum

```c
int convertHexStrToNum(unsigned long *num, char *hexStr);
```

This function converts a hexadecimal string to a number. It uses the strtoul function for the conversion.

* `num`: A pointer to the variable where the resulting number will be stored.
* `hexStr`: The hexadecimal string to convert.

Returns 1 if the conversion is successful, or 0 if the string contains invalid characters.

### manualConvertionHexStrToNum

```c
int manualConvertionHexStrToNum(unsigned long *num, char *hexStr);
```

This function manually converts a hexadecimal string to a number.

* `num`: A pointer to the variable where the resulting number will be stored.
* `hexStr`: The hexadecimal string to convert.

Returns 1 if the conversion is successful, or 0 if the string contains invalid characters.

## Compilation and Testing

To compile the code, use the following command:

```bash
make
```

To run the tests, execute the compiled program:

```bash
./hexconverter
```
