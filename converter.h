#ifndef CONVERTER_H
#define CONVERTER_H

#include <stddef.h>

// Convert a number to a hexadecimal string
int convertNumToHexStr(char *hexStr, size_t bufferSize, unsigned long num);

// Convert a number to a hexadecimal string manually
int manualConvertionNumToHexStr(char *hexStr, size_t bufferSize, unsigned long num);

// Convert a hexadecimal string to a number
int convertHexStrToNum(unsigned long *num, char *hexStr);

// Convert a hexadecimal string to a number manually
int manualConvertionHexStrToNum(unsigned long *num, char *hexStr);

#endif // CONVERTER_H