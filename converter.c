#include "converter.h"
#include <stdio.h>  // For snprintf
#include <stdlib.h> // For strtoul

int convertNumToHexStr(char *hexStr, size_t bufferSize, unsigned long num) {
    size_t requiredSize = snprintf(NULL, 0, "%lX", num) + 1; // +1 for null terminator

    // Check if the buffer is large enough
    if (bufferSize < requiredSize) {
        return 0;
    }

    return snprintf(hexStr, bufferSize, "%lX", num);
}

int manualConvertionNumToHexStr(char *hexStr, size_t bufferSize, unsigned long num) {
    // Check if the buffer is large enough
    if (bufferSize < 9) {
        return 0;
    }

    // Convert the number to a hexadecimal string manually
    return snprintf(hexStr, bufferSize, "%02lX%02lX%02lX%02lX", 
        (num >> 24) & 0xFF, 
        (num >> 16) & 0xFF, 
        (num >> 8) & 0xFF, 
        num & 0xFF);
}

int convertHexStrToNum(unsigned long *num, char *hexStr) {
    char *endptr;
    *num = strtoul(hexStr, &endptr, 16); // Base 16 for hexadecimal

    // Check if the conversion was successful
    if (*endptr != '\0') {
        return 0;
    }

    return 1;
}

int manualConvertionHexStrToNum(unsigned long *num, char *hexStr) {
    unsigned long result = 0;
    char ch;

    // Iterate through the string
    while ((ch = *hexStr++) != '\0') {
        // Convert the character to a digit
        unsigned int digit = 0;
        if(ch >= '0' && ch <= '9') {
            digit += ch - '0';
        } else if(ch >= 'A' && ch <= 'F') {
            digit += ch - 'A' + 10;
        } else if(ch >= 'a' && ch <= 'f') {
            digit += ch - 'a' + 10;
        } else {
            // Invalid character
            return 0;
        }

        // Shift the result left by 4 bits and add the digit
        result = (result << 4) | digit;
    }

    // Set the num parameter to the result
    *num = result;

    return 1;
}